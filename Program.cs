using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Specialized;
using System.Reflection;

namespace ConsoleApp26
{
    class Program
    {
        static void Main(string[] args)
        {
            var pol = new Polynom3(@"C:\Users\неуч\Desktop\pol\1.txt");
            var pol_2 = new Polynom3(@"C:\Users\неуч\Desktop\pol\2.txt");

            Console.WriteLine(pol + " - pol");

            Console.WriteLine("\n\n" + pol_2 + " - pol_2");

            pol.Add(pol_2);
            Console.WriteLine("\n\n" + pol + " - Добавили pol_2");

            pol.Delete(2, 7, 0);
            Console.WriteLine("\n\n" + pol + " - Удалили что-то \n\n");

            pol.Derivate(1);

            Console.WriteLine("\n\n" + pol.Value(1, 1, 1) + "\n\n");

            pol.Insert(1, 1, 1, 1);
            Console.WriteLine(pol);

            pol.Insert(2, 2, 2, 2);
            Console.WriteLine("\n\n" + pol);

            var temp = pol.MinCoef();
            foreach (var a in temp)
                Console.WriteLine(a);
        }
    }
}
