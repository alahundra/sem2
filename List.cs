using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp23
{
    class Program
    {
        static void Main(string[] args)
        {
             List<int> numbers = new List<int> { 4, 2, 9, 1, 0, 737, 99 };
             
             numbers.Add(56);      // добавление элементов по одному(для себя)
             numbers.Add(55);
             numbers.Add(2);
             numbers.Add(2225);
             numbers.Add(7373);

            Console.WriteLine("Изначальный список:");

            foreach(var item in numbers)  // вывод элементов
                Console.WriteLine(item);
            
           //есть ли элемент в списке 
            Console.Write("Введите элемент, который нужно проверить на нахождение в списке(только число): ");
            var N = Convert.ToInt32(Console.ReadLine());
            var indexOfIntegerValue = numbers.IndexOf(N);   //только индекс первого вхождения
            if((indexOfIntegerValue) >= 0)
                 Console.WriteLine("Данный элемент присутствует в списке");
            else 
                 Console.WriteLine("Данный элемент отсутствует в списке");


            //поиск по порядковому номеру
            Console.Write("Введите порядковый номер элемента: ");
            var temp = Convert.ToInt32(Console.ReadLine());
            if (temp > numbers.Count)
                Console.WriteLine("Выход за границу списка");
            else
                Console.WriteLine(numbers.ElementAt(temp - 1)); // numbers.ElementAt(temp-1) - вывод элементов списка numbers, стоящих на temp-1(т.к отсчет в списках начинается с 0) позициии
            


            //объединение двух списков за O(n)
            Console.WriteLine("Объединение двух списков:");
            List<int> numbers1 = new List<int> { 4, 2, 9, 1, 92, 737, 99 };
            Console.WriteLine("Изначальный список №2:");
            foreach (var items1 in numbers1)      
                Console.WriteLine(items1);
            foreach(var item in numbers.Union(numbers1))  //numbers.Union(numbers1) - объединение двух списков, на первое место список перед функцией 
                Console.WriteLine(item);

            //разность двух списков за O(n)
            Console.WriteLine("Разность двух списков:");
            foreach(var item in numbers.Except(numbers1)) //numbers.Except(numbers1) - из array numbers удаляет все элементы, которые есть в numbers1
                Console.WriteLine(item);

            //пересечение двух списков за O(n)
            Console.WriteLine("Пересечение двух списков:");
            foreach(var item in numbers.Intersect(numbers1))   //numbers.Intersect(numbers1) - находит общий элемент
                Console.WriteLine(item);
        
        }
    }
}
